import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module201.json";
import Docs2013 from "@/src/components/course-modules/201/Docs2013.mdx";

export default function Lesson2013() {
  const slug = "2013";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={201} sltId="201.3" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2013 />
    </LessonLayout>
  );
}
