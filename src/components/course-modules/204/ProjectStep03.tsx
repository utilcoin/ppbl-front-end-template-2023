import ProjectLayout from "@/src/components/lms/Lesson/ProjectLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module204.json";
import DocsStep03 from "@/src/components/course-modules/204/DocsStep03.mdx";

export default function ProjectStep03() {
  const slug = "project-step-03";
  const title = "Step 3: Lock Tokens in Faucet"

  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <ProjectLayout moduleNumber={204} title={title} sltId="102.4" slug={slug}>
      <DocsStep03 />
    </ProjectLayout>
  );
}
